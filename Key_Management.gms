
*we define the different sets needed.
*A set of the 30 keys and a set of 8 nodes.
*we also need an alias of the set of keys.

SETS
         k set of keys /k1*k30/
         i set of nodes/1*8/;

ALIAS(i,j);

*We implement the data given in the assignment.
*We have the memory of a node, the space a key is occupying in a node, the
*number of keys that must be shared between two nodes in order for them to
*communicate directly and the number of times a key can be used.

SCALARS
         m memory /700/
         o occupying /186/
         s share /3/
         u usage /4/;

*Now we define the variables needed in order to model the problem.

*We need a free variable z which is the value of the objective function.

*We ned a binary variable x(i,j) that is 1 if and only if node i and node j
*is communicating directly.

*We ned a binary variable y(k,i) that is 1 if and only if key k is assigned to
*node i.

*At last we need a binary variable q(k,i,j) that is 1 if and only if key k is
*assigned to both node i and node j.

VARIABLES

z      mumber of directly communicating nodes
x(i,j) if node i and node j communicate directly
y(k,i) if key k is assigned to node i
q(k,i,j) if node i and j has note k in common   ;

BINARY VARIABLES

x(i,j)
y(k,i)
q(k,i,j);

*Next we define the equations that our mathematical model will consist of.

*The objective is that of maximizing the total number of direct communications
*between nodes.

*We need a constraint saying what it means for two nodes to communicate directly.
*In the expression i made there was a product of two variables so i had to
*linealize the expression which is why we need the variable q(k,i,j).
*We have the linealization constraints stating that if a key is assigned to both
*node i an node j it is assigned to node i and it is assigned to node j.
*And also the other way around.
*Then we can make the constraint saying that for two different nodes, the nodes
*can communicate directly if at least three of the keys is assigned to both
*nodes.

*We need a constraint stating that each key can at most be assigned to a
*certain number of nodes.

*We need a constraint stating that a node cannot be assigned more keys than
*its memory allows.

EQUATIONS

obj                     maximizing the total number of directly communicating nodes
lin1(k,i,j)             first linearizing
lin2(k,i,j)             second linearizing
lin3(k,i,j)             third linearizing
communication(i,j)      nodes must have 3 keys in common in order to communicate directly
keys(k)                 a key can only be used a certain number of times
memory(i)               a node cant be assigned more keys than its memory allows;

obj..                   z =e= sum((i,j)$(ord(i) > ord(j)),x(i,j));
lin1(k,i,j)..           q(k,i,j) =l= y(k,i) ;
lin2(k,i,j)..           q(k,i,j) =l= y(k,j) ;
lin3(k,i,j)..           y(k,i)+y(k,j) =l= 1+q(k,i,j) ;
communication(i,j)$(ord(i) <> ord(j))..    x(i,j)*3 =l= sum(k,q(k,i,j));

keys(k)..               sum(i,y(k,i)) =l= u;

memory(i)..             sum(k,y(k,i))*o =l= m ;

MODEL security /all/;

SOLVE security USING MIP MAXIMIZING z;

display x.l,y.l;




