
*we define the different sets needed. A set of the 25 player, a set of the
*different roles a player can have in a formation and the set of formations.

*We also creat the two subset that is the quality and strength players.

SETS
         i set of players          /P1*P25/
         j set of roles            /GK,CDF,LB,RB,CMF,LW,RW,OMF,CFW,SFW/
         k set of formations       /442,352,4312,433,343,4321/
         quality(i) quality players of i /P13,P20,P21,P22/
         strength(i) strength players of i /P10,P12,P23/;

*We implement the two tables from the assignment. First the table saying how
*well all players fits into the different roles on a scale from 0 to 10.

*Second the table saying how many players in each role is needed in each of the
*different formations.

TABLE    f(i,j) fitness of player i in role j

         GK      CDF     LB      RB      CMF     LW      RW      OMF     CFW     SFW
P1       10      0       0       0       0       0       0       0       0       0
P2       9       0       0       0       0       0       0       0       0       0
P3       8.5     0       0       0       0       0       0       0       0       0
P4       0       8       6       5       4       2       2       0       0       0
P5       0       9       7       3       2       0       2       0       0       0
P6       0       8       7       7       3       2       2       0       0       0
P7       0       6       8       8       0       6       6       0       0       0
P8       0       4       5       9       0       6       6       0       0       0
P9       0       5       9       4       0       7       2       0       0       0
P10      0       4       2       2       9       2       2       0       0       0
P11      0       3       1       1       8       1       1       4       0       0
P12      0       3       0       2       10      1       1       0       0       0
P13      0       0       0       0       7       0       0       10      6       0
P14      0       0       0       0       4       8       6       5       0       0
P15      0       0       0       0       4       6       9       6       0       0
P16      0       0       0       0       0       7       3       0       0       0
P17      0       0       0       0       3       0       9       0       0       0
P18      0       0       0       0       0       0       0       6       9       6
P19      0       0       0       0       0       0       0       5       8       7
P20      0       0       0       0       0       0       0       4       4       10
P21      0       0       0       0       0       0       0       3       9       9
P22      0       0       0       0       0       0       0       0       8       8
P23      0       3       1       1       8       4       3       5       0       0
P24      0       3       2       4       7       6       5       6       4       0
P25      0       4       2       2       6       7       5       2       2       0 ;

TABLE    n(j,k) number of players on position j in formation k

         442     352     4312    433     343     4321
GK       1       1       1       1       1       1
CDF      2       3       2       2       3       2
LB       1       0       1       1       0       1
RB       1       0       1       1       0       1
CMF      2       3       3       3       2       3
LW       1       1       0       0       1       0
RW       1       1       0       0       1       0
OMF      0       0       1       0       0       2
CFW      2       1       2       1       1       1
SFW      0       1       0       2       2       0 ;

*Now we define the variables needed in order to model the problem.

*We need a free variable z which is the value of the objective function.

*We ned a binary variable x(i,j,k) that is 1 if and only if player i plays
*position j in formation k.

*At last we need a binary variable y(k) that is 1 if and only if the k'th
*formation is chosen in the solution.

VARIABLES

z        the value of the objective in total fitness
x(i,j,k) if player i plays position j in formation k
y(k)     if formation k is chosen ;

BINARY VARIABLES

x(i,j,k)
y(k) ;

*Next we define the equations that our mathematical model will consist of.

*The objective is that of maximizing the total fitness when choosing players
*which is the sum over players and roles of the fitness f(i,j) multiplied by
*the sum over formations of the variable x(i,j,k).

*We need a constraint stating that only one formation can be used in the
*solution.

*We need a constraint stating that for the chosen formation we must satisfy
*the required number of players in the different roles from table n(j,k).

*We need a constraint stating that a player can at most be assigned one role in
*a formation.

*We need a constraint stating that the solution must include at least one
*quality player.

*We need a constraint stating that if all quality players is used we also have
*to use at least one strength player.

EQUATIONS

obj              the total fitness
oneformation     only one formation can be used
fullteam(j,k)    the required number of players must be met
oneposition(i)   a player can only take one position in the lineup
onequality       we need at least one quality player in our formation
balance          balance of quality and strength players ;

obj..            z =e= sum((i,j),f(i,j)*sum(k,x(i,j,k))) ;

oneformation..   sum(k,y(k)) =e= 1 ;

fullteam(j,k)..  sum(i,x(i,j,k)) =e= y(k)*n(j,k) ;

oneposition(i).. sum((j,k),x(i,j,k)) =l= 1 ;

onequality..     sum((j,k),sum(i$quality(i), x(i,j,k))) =g= 1 ;

balance..        sum((j,k),sum(i$quality(i), x(i,j,k))) =l= 3+sum((j,k),sum(i$strength(i), x(i,j,k))) ;

MODEL soccer /all/;

SOLVE soccer USING MIP MAXIMIZING z;

display x.l;
