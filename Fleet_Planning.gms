
*we define the different sets needed. A set of the 5 ships, a set of the
*8 ports and a set of the 2 routes.

*We also create 2 subset of the ports that is the 2 sets of incompatible ports.

SETS
         v set of ships  /v1*v5/
         p set of ports  /Singapore,Incheon,Shanghai,Sydney,Gladstone,Dalian,Osaka,Victoria/
         r set of routes /Asia,ChinaPacific/
         h1(p) first pair/Singapore,Osaka/
         h2(p) second pair/Incheon, Victoria/;

*We implement the data given in the assignment.
*First we have 3 parameters. We have the prize in millions of using
*the different ships and the number of days in a year we can use the ships
*we have paid for. Last we have a parameter saying how many times we must
*visit a port p if we choose to service port p.

PARAMETERS

         f(v) prize of using ship v /v1 65
                                     v2 60
                                     v3 92
                                     v4 100
                                     v5 110/

         g(v) days ship v can be used /v1 300
                                       v2 250
                                       v3 350
                                       v4 330
                                       v5 300/

         d(p) times we must visit port p /Singapore 15
                                          Incheon   18
                                          Shanghai  32
                                          Sydney    32
                                          Gladstone 45
                                          Dalian    32
                                          Osaka     15
                                          Victoria  18/

*Next we have 3 tables. The first table shows the cost in millions of having
*the different ships sailing the 2 routes. Next table shows the number of days
*it takes for the different ships to sail the 2 routes. The last table shows
*which ports that can be visited on the 2 routes.

TABLE    c(v,r)  cost of sailing route r with ship v

         Asia    ChinaPacific
v1       1.41    1.9
v2       3       1.5
v3       0.4     0.8
v4       0.5     0.7
v5       0.7     0.8

TABLE    t(v,r)  number of days for ship v to sail route r

         Asia    ChinaPacific
v1       14.4    21.2
v2       13      20.7
v3       14.4    20.6
v4       13      19.2
v5       12      20.1

TABLE    a(p,r)  indicating if port p is on route r

                 Asia    ChinaPacific
Singapore        1       0
Incheon          1       0
Shanghai         1       1
Sydney           0       1
Gladstone        0       1
Dalian           1       1
Osaka            1       0
Victoria         0       1 ;

*We also need a big M scalar for the model.

SCALAR     M big M /1000000/;

*Now we define the variables needed in order to model the problem.

*We need a free variable z which is the value of the objective function.

*We need a binary variable y(p) that is 1 if and only if port p is serviced.

*We need a binary variable k(v) that is 1 if and only if ship v is used.

*We need an integer variable q(p,v,r) which is the number of times port p is
*visited by ship v sailing route r.

*We need an integer variable x(v,r) which is the number of times ship v sails
*route r.

VARIABLES

z the total cost of serving costumers
y(p) if port p is serviced
k(v) if ship v is used
q(p,v,r) the number of times port p is visited by ship v sailing route r
x(v,r) the number of times ship v sails route r;

INTEGER VARIABLES

q(p,v,r)
x(v,r);

BINARY VARIABLES

y(p)
k(v);

EQUATIONS

*Next we define the equations that our mathematical model will consist of.

*The objective is that of minimizing the total cost of serving costumers.
*This means adding the cost of bying ships and sailing routes with them.

*We need a constraint stating that we have to pay for using a ship if we want
*to use it.

*We need a constraint stating that if we choose to service a port p we must
*visit it the number of times shown in d(p).

*We need a constraint stating that in order for a ship v to be visiting a port p
*by sailing route r then ship v must actualy sail route r.

*We need a constraint stating that a ship can only be used up to the number of
*days given by g(v).

*We need a constraint stating that we have to service at least five ports.

*We need 2 constraint stating that we cannot service 2 ports that are
*incompatible.


obj              total cost of serving costumers
buy(v)           we must buy a ship in order to use it
visit(p)         if a port is serviced we must visit the required number of times
sailing(v,r,p)   a ship must sail in order to visit a port
workload(v)      a ship can only be used a certain number of days
deal             we have to visit a certain number of the different ports
par1             can only visit one of the ports in first pair
par2             can only visit one of the ports in second pair ;


obj..            z =e= sum(v,k(v)*f(v)*1000000+sum(r,1000000*c(v,r)*x(v,r)));

buy(v)..         k(v)*M =g= sum((p,r), q(p,v,r));

visit(p)..       sum((v,r),q(p,v,r)*a(p,r)) =g= d(p)*y(p);

sailing(v,r,p).. x(v,r) =g= q(p,v,r);

workload(v)..    sum(r,x(v,r)*t(v,r)) =l= g(v);

deal..           sum(p,y(p)) =g= 5;

par1..           sum(p$h1(p),y(p)) =l= 1;

par2..           sum(p$h2(p),y(p)) =l= 1;

MODEL shipping /all/;


SOLVE shipping USING MIP MINIMIZING z;

display y.l,k.l,q.l,x.l ;